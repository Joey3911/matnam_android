package com.soma.second.matnam.ui.models;

import android.graphics.Bitmap;

/**
 * Created by Dongjun on 15. 11. 2..
 */
public class Food {

    private String imgUrl;
    private String standardUrl;

    public Food(String _imgUrl, String standardUrl) {
        this.imgUrl = _imgUrl;
        this.standardUrl = standardUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getStandardUrl() {
        return standardUrl;
    }

    public void setStandardUrl(String standardUrl) {
        this.standardUrl = standardUrl;
    }
}
