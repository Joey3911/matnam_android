package com.soma.second.matnam.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.MatnamApi;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.PlaceRecord;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.soma.second.matnam.R;
import com.soma.second.matnam.Utils.CloudEndpointBuildHelper;
import com.soma.second.matnam.Utils.InstagramRestClient;
import com.soma.second.matnam.ui.adapters.FoodDetailGridAdapter;
import com.soma.second.matnam.ui.advrecyclerview.LikeListActivity;
import com.soma.second.matnam.ui.advrecyclerview.data.LikeRoomDataProvider;
import com.soma.second.matnam.ui.models.Food;
import com.soma.second.matnam.ui.widget.ExpandableHeightGridView;
import com.soma.second.matnam.ui.widget.Indicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import cz.msebera.android.httpclient.Header;

public class FoodDetailActivity extends BaseActivity implements View.OnClickListener {

    MatnamApi matnamApi = null;
    Indicator mIndicator;

    ExpandableHeightGridView gridView;
    ArrayList<Food> foodArray = new ArrayList<Food>();
    FoodDetailGridAdapter foodDetailGridAdapter;

    public static final String FOOD_ID = "id";
    public static final String FOOD_KEYWORD = "keyword";
    public static final String FOOD_IMG_URL = "url";

    private long placeId;

    @InjectView(R.id.image)
    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0, 0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fooddetail);
        mIndicator = new Indicator(this);

        findViewById(R.id.like_fab).setOnClickListener(this);

        ButterKnife.inject(this);
        mBackground = mImageView;

        gridView = (ExpandableHeightGridView) findViewById(R.id.food_grid);
        gridView.setExpanded(true);
        foodDetailGridAdapter = new FoodDetailGridAdapter(this, R.layout.item_food, foodArray);
        gridView.setAdapter(foodDetailGridAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                String standardUrl = foodDetailGridAdapter.getItem(position).getStandardUrl();
                showImageDialog(standardUrl);            // pass the image to a method to create your dialog
            }
        });

        placeId = getIntent().getExtras().getLong(FOOD_ID);
        String keyword = getIntent().getExtras().getString(FOOD_KEYWORD);
        String imageUrl = getIntent().getExtras().getString(FOOD_IMG_URL);
        LikeRoomDataProvider.FOOD_IMG_URL = imageUrl;

        new loadPlaceAsyncTask().execute(placeId);

        Glide.with(this).load(imageUrl).into((ImageView) findViewById(R.id.image));

        InstagramRestClient.get(InstagramRestClient.tagMediaRecent(keyword), null, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                if (!mIndicator.isShowing())
                    mIndicator.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (mIndicator.isShowing())
                    mIndicator.hide();

                try {
                    JSONArray dataArr = response.getJSONArray("data");
                    for (int i = 0; i < dataArr.length(); i++) {
                        JSONObject data = (JSONObject) dataArr.get(i);
                        String thumbnail_url = data.getJSONObject("images").getJSONObject("thumbnail").getString("url");
                        String standard_url = data.getJSONObject("images").getJSONObject("standard_resolution").getString("url");

                        foodDetailGridAdapter.add(new Food(thumbnail_url, standard_url));
                        if (foodDetailGridAdapter.getCount() > 5) {
                            foodDetailGridAdapter.notifyDataSetChanged();
                            if (mIndicator.isShowing())
                                mIndicator.hide();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable e) {
                if (mIndicator.isShowing())
                    mIndicator.hide();
            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.like_fab :
                LikeRoomDataProvider.PLACE_ID = placeId;
                Intent intent = new Intent(FoodDetailActivity.this, LikeListActivity.class);
                intent.putExtra("placeId", placeId);
                startActivity(intent);
                break;
        }
    }

    class loadPlaceAsyncTask extends AsyncTask<Long, Void, PlaceRecord>{

        @Override
        protected PlaceRecord doInBackground(Long... params) {
            if(matnamApi==null){
                matnamApi = CloudEndpointBuildHelper.getEndpoints();
            }

            PlaceRecord placeRecord = null;
            try {
                placeRecord = matnamApi.getPlace(params[0]).execute();
            } catch (IOException e) {
                Log.e("API", "Error"+e.getMessage());
                e.printStackTrace();
            }
            return placeRecord;
        }

        @Override
        protected void onPostExecute(PlaceRecord result) {
            super.onPostExecute(result);

            TextView nameTextView = (TextView) findViewById(R.id.food_name);
            nameTextView.setText(result.getName());

            TextView categoryTextView = (TextView) findViewById(R.id.food_category);
            categoryTextView.setText(result.getCategory());

            TextView descriptionTextView = (TextView) findViewById(R.id.food_description);
            descriptionTextView.setText("소개 : " + result.getDescription());

            TextView locationTextView = (TextView) findViewById(R.id.food_location);
            locationTextView.setText("위치 : " + result.getLocation());

            TextView priceTextView = (TextView) findViewById(R.id.food_price);
            priceTextView.setText("가격대 : " + result.getPrice());

            TextView openingTextView = (TextView) findViewById(R.id.food_opening);
            openingTextView.setText("영업시간 : " + result.getOpening());

            TextView telTextView = (TextView) findViewById(R.id.food_tel);
            telTextView.setText("전화번호 : " + result.getTel());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if ( !mIndicator.isShowing())
                mIndicator.show();
        }
    }

    public void showImageDialog(String imageUrl){
        final Dialog imageDialog = new Dialog(this);

        imageDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        imageDialog.setContentView(getLayoutInflater().inflate(R.layout.dialog_image
                , null));
        ImageView imageview = (ImageView)imageDialog.findViewById(R.id.big_food_image);
        Glide.with(this).load(imageUrl).into(imageview);
        imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog.dismiss();
            }
        });
        imageDialog.show();
    }

}


