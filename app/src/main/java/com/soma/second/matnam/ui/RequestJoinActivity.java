package com.soma.second.matnam.ui;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.kimyoungjoon.myapplication.backend.matnamApi.MatnamApi;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.LikeRoomRecord;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.RequestJoinRecord;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.soma.second.matnam.R;
import com.soma.second.matnam.Utils.CloudEndpointBuildHelper;
import com.soma.second.matnam.listdubbies.provider.DataProvider;
import com.soma.second.matnam.ui.adapters.InstagramFollowerListAdapter;
import com.soma.second.matnam.ui.models.InstagramFollwer;
import com.soma.second.matnam.ui.models.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RequestJoinActivity extends AppCompatActivity implements View.OnClickListener {

    MatnamApi matnamApi = null;

    public static String ROOM_MEMBERS_ID = "room_members_id";
    public static String ROOM_ID = "room_id";

    GridView gridView;
    InstagramFollowerListAdapter registerFollowerGridAdapter;
    InstagramFollowerListAdapter instagramFollowerListAdapter;

    private String groupId;

    private long roodId;
    private String roomMembersId;
    private int memberCount;
    private ArrayList<String> requestMemberList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_with_friend);
        init();

        ArrayList<InstagramFollwer> instagramFollwerList = new ArrayList<>();
        instagramFollwerList.add(new InstagramFollwer(User.getId(), User.getFullName(), User.getUserName(), User.getProfileImgUrl()));

        gridView = (GridView) findViewById(R.id.friend_grid);
        registerFollowerGridAdapter = new InstagramFollowerListAdapter(this, R.layout.item_insta_follower_grid, instagramFollwerList);
        gridView.setAdapter(registerFollowerGridAdapter);

        gridView.setOnItemClickListener(mItemClickListener);

        instagramFollowerListAdapter = new InstagramFollowerListAdapter(this, R.layout.item_insta_follower_list, DataProvider.instagramFollwerList);
    }

    private void init() {

        roodId = getIntent().getExtras().getLong(ROOM_ID);
        roomMembersId = getIntent().getExtras().getString(ROOM_MEMBERS_ID);

        requestMemberList = new ArrayList<>();
        requestMemberList.add(User.getId());

        memberCount = roomMembersId.split(",").length;
//        groupId = getIntent().getExtras().getString("groupId");

        setOnClickViews();
    }

    private void setOnClickViews() {
        findViewById(R.id.friend_search_text).setOnClickListener(this);
        findViewById(R.id.friend_request_text).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.friend_search_text:
                DialogPlus dialog = DialogPlus.newDialog(this)
                        .setAdapter(instagramFollowerListAdapter)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                addRegisterUser((InstagramFollwer) item);
                            }
                        })
                        .setExpanded(true)  // This will enable the expand feature, (similar to android L share dialog)
                        .setHeader(R.layout.dialogplus_header_search)
                        .create();
                dialog.show();
                break;
            case R.id.friend_request_text :
                if(registerFollowerGridAdapter.getCount() == memberCount) {

                    String requestMembersId = requestMemberList.toString().substring(1, requestMemberList.toString().length()-1);
                    Log.e("TEST", roodId + " " + roomMembersId + " " + requestMembersId);

                    RequestJoinRecord requestJoinRecord = new RequestJoinRecord();
                    requestJoinRecord.setRoomId(roodId);
                    requestJoinRecord.setReceiveMembersId(roomMembersId);
                    requestJoinRecord.setRequestMembersId(requestMembersId);

                    new addRequestJoinDataAsyncTask().execute(requestJoinRecord);
                } else {
                    Toast.makeText(getApplicationContext(), "인원 수를 맞춰주세요!", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void addRegisterUser(final InstagramFollwer _user) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int regCount = registerFollowerGridAdapter.getCount();
                if(regCount < memberCount) {
                    requestMemberList.add(_user.getId());
                    registerFollowerGridAdapter.add(_user);
                    registerFollowerGridAdapter.notifyDataSetChanged();
                } else {
                    new SweetAlertDialog(RequestJoinActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("가능 인원 수를 초과하였습니다.")
                            .setContentText((memberCount-1) + "명의 친구들을 초대하여 방을 구성해주세요.")
                            .show();
                }
            }
        });
    }

    private AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long l_position) {
            // parent는 AdapterView의 속성의 모두 사용 할 수 있다.

            if (position == 0)
                Toast.makeText(getApplicationContext(), "자신을 제외할 수 없습니다.", Toast.LENGTH_SHORT).show();
            else {
                requestMemberList.remove(registerFollowerGridAdapter.getItem(position).getId());
                registerFollowerGridAdapter.remove(registerFollowerGridAdapter.getItem(position));
                registerFollowerGridAdapter.notifyDataSetChanged();
            }

        }
    };

    class addRequestJoinDataAsyncTask extends AsyncTask<RequestJoinRecord, Void, RequestJoinRecord> {

        List<LikeRoomRecord> likeRoomRecordList;

        @Override
        protected RequestJoinRecord doInBackground(RequestJoinRecord... params) {
            if (matnamApi == null) {
                matnamApi = CloudEndpointBuildHelper.getEndpoints();
            }

            RequestJoinRecord requestJoinRecord = params[0];

            try {
                //JoinRequestListDialog.myRoomJoinRequest.add(params[0]);
                //Log.v("params[0]", params[0].toString());
                //Log.v("dp.joinRequest", JoinRequestListDialog.myRoomJoinRequest.toString());
                Log.v("joinRequestData2", requestJoinRecord.toString());
                matnamApi.addRequestJoin(requestJoinRecord).execute();

                String requestMembersId = requestJoinRecord.getRequestMembersId();
                String receiveMembersId = requestJoinRecord.getReceiveMembersId();
                String[] memberIdArr = receiveMembersId.split(",");
                for(String memberId : memberIdArr) {
                    try {
                        matnamApi.messaging().sendMessage(requestMembersId, memberId).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            } catch (IOException e) {
                Log.e("API", "Error" + e.getMessage());
                e.printStackTrace();
            }
            return requestJoinRecord;
        }

        @Override
        protected void onPostExecute(RequestJoinRecord resultList) {
            super.onPostExecute(resultList);

                new SweetAlertDialog(RequestJoinActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("신청되었습니다!")
                        .setContentText("답변이 오기까지 기다려주세요.")
                        .setConfirmText("확인")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                finish();
                            }
                        })
                        .show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

}