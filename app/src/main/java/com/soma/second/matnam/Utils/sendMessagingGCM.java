package com.soma.second.matnam.Utils;

import android.os.AsyncTask;

import com.example.kimyoungjoon.myapplication.backend.matnamApi.MatnamApi;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.UserRecord;

import java.io.IOException;


/**
 * Created by Dongjun on 15. 11. 16..
 */
public class sendMessagingGCM extends AsyncTask<String, Void, String> {

    String mMessage;

    public sendMessagingGCM(String _message) {
        this.mMessage = _message;
    }

    MatnamApi matnamApi = null;

    @Override
    protected String doInBackground(String... params) {

        if (matnamApi == null) {
            matnamApi = CloudEndpointBuildHelper.getEndpoints();
        }

        String instaId = params[0], token = "";
        try {
            UserRecord userRecord = matnamApi.getUser(instaId).execute();
            token = userRecord.getToken();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return token;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        try {
            matnamApi.messaging().sendMessage(mMessage, result);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        if(result != null)

    }
}
