package com.soma.second.matnam.ui.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.example.kimyoungjoon.myapplication.backend.matnamApi.MatnamApi;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.LikeRoomRecord;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.RequestJoinRecord;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.RequestJoinRecordCollection;
import com.soma.second.matnam.R;
import com.soma.second.matnam.Utils.CloudEndpointBuildHelper;
import com.soma.second.matnam.listdubbies.provider.DataProvider;
import com.soma.second.matnam.ui.ReceiveJoinActivity;
import com.soma.second.matnam.ui.adapters.joinRequestListDialogAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.Intent;

/**
 * Created by youngjoosuh on 2015. 11. 13..
 */

public class JoinRequestListDialog extends Dialog {

    int position;
    long room_id;
    LikeRoomRecord selectedRoomRecord;
    String room_title;

    static final float[] DIMENSIONS_LANDSCAPE = { 460, 260 };
    static final float[] DIMENSIONS_PORTRAIT = { 320, 420 };
    static final FrameLayout.LayoutParams FILL = new FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.FILL_PARENT,
            ViewGroup.LayoutParams.FILL_PARENT);
    static final int MARGIN = 4;
    static final int PADDING = 2;

    private LinearLayout mContent;
    private TextView textView;
    private ListView listView;
    List<RequestJoinRecord> requestJoinRecordList;
    MatnamApi matnamApi = null;
    private Context context;

    public JoinRequestListDialog(Context context, int position) {
        super(context);
        this.position = position;
    }
    public JoinRequestListDialog(Context context, long room_id) {
        super(context);
        this.room_id = room_id;
    }

    public JoinRequestListDialog(Context context, LikeRoomRecord selectedRoomRecord) {
        super(context);
        context = context;
        this.selectedRoomRecord = selectedRoomRecord;
        this.room_id = selectedRoomRecord.getId();
        this.room_title = selectedRoomRecord.getTitle();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContent = new LinearLayout(getContext());
        mContent.setOrientation(LinearLayout.VERTICAL);

        Log.v("selectedRoomRecord", "" + selectedRoomRecord);
        Log.v("room_id", "" + room_id);
        Log.v("room_title", "" + room_title);


        new loadRequestJoinRecordsAsyncTask().execute();
        Log.v("dp.joinRecordListtttt", DataProvider.requestJoinRecordList.toString());

        /*for(int i=0; i<DataProvider.requestJoinRecordList.size(); i++) {
            requestJoinRecordList.add(DataProvider.requestJoinRecordList.get(i));
        }*/
        //setUpTitle();
        setListView();

        Display display = getWindow().getWindowManager().getDefaultDisplay();
        final float scale = getContext().getResources().getDisplayMetrics().density;
        float[] dimensions = (display.getWidth() < display.getHeight()) ? DIMENSIONS_PORTRAIT
                : DIMENSIONS_LANDSCAPE;

        addContentView(mContent, new FrameLayout.LayoutParams(
                (int) (dimensions[0] * scale + 0.5f), (int) (dimensions[1]
                * scale + 0.5f)));
    }

    /*private void setUpTitle() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        textView = new TextView(getContext());
        textView.setText("이 방에 들어온 Join 요청");
        textView.setTextColor(Color.WHITE);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setBackgroundColor(getContext().getResources().getColor(R.color.deep_orange_400));
        textView.setPadding(MARGIN + PADDING, MARGIN, MARGIN, MARGIN);
        mContent.addView(textView);
    }*/

    private void setListView() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        linearLayout.setLayoutParams(params);
        linearLayout.setPadding(20, 20, 20, 20);
        linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);

        textView = new TextView(getContext());
        textView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        textView.setText("이 방에 들어온 Join 요청");
        textView.setTextColor(Color.BLACK);
        textView.setTextSize(25);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        linearLayout.addView(textView);

        listView = new ListView(getContext());
        listView.setPadding(20, 20, 20, 20);
        listView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        listView.setBackgroundColor(getContext().getResources().getColor(R.color.deep_orange_400));
        linearLayout.addView(listView);



        mContent.addView(linearLayout);
    }

    class loadRequestJoinRecordsAsyncTask extends AsyncTask<Void, Void, List<RequestJoinRecord>> {

        @Override
        protected List<RequestJoinRecord> doInBackground(Void... params) {

            if (matnamApi == null) {
                matnamApi = CloudEndpointBuildHelper.getEndpoints();
            }

            RequestJoinRecordCollection requestJoinRecordCollection = null;
            RequestJoinRecord data = null;
            List<RequestJoinRecord> requestJoinRecordList = new ArrayList<>();
            int count = 0;

            try {
                requestJoinRecordCollection = matnamApi.getRequestJoins().execute();
                if(requestJoinRecordCollection==null) Log.v("joinRequestData3", "NULL");
                else {
                    for(int i=0; i<requestJoinRecordCollection.getItems().size(); i++) {
                        Log.v("joinRequestData3", requestJoinRecordCollection.getItems().get(i).toString());
                    }

                }

                Log.v("collectionsize:", ""+requestJoinRecordCollection.getItems().size());
                for(int i=0; i<requestJoinRecordCollection.getItems().size(); i++) {
                    data = requestJoinRecordCollection.getItems().get(i); //one join request
                    Log.v("room_id", ""+room_id);
                    Log.v("data.getRoomId", ""+data.getRoomId());
                    if(data.getRoomId()==room_id) {
                        count++;
                        requestJoinRecordList.add(data);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return requestJoinRecordList;
        }

        @Override
        protected void onPostExecute( List<RequestJoinRecord> result) {
            super.onPostExecute(result);
            if(result!=null){
                Log.v("result", ""+result.size());
                DataProvider.requestJoinRecordList = result;
                for(int i=0; i<result.size(); i++) {
                    //DataProvider.requestJoinRecordList.add(result.get(i));
                    Log.v("dp.JoinRecordList(i)", "" + DataProvider.requestJoinRecordList.get(i));
                }
                Log.v("dp.JoinRecordList", "" + DataProvider.requestJoinRecordList.size());

                listView.setAdapter(new joinRequestListDialogAdapter(getContext(), DataProvider.requestJoinRecordList));
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(getContext(), ReceiveJoinActivity.class);
                        Log.v("membersId111", DataProvider.requestJoinRecordList.get(position).getRoomId().toString());
                        Log.v("membersId111", DataProvider.requestJoinRecordList.get(position).getRequestMembersId());
                        intent.putExtra("membersId", DataProvider.requestJoinRecordList.get(position).getRequestMembersId());
                        getContext().startActivity(intent);
                    }
                });
            }
            else if(result==null) Log.v("result", "result is NULL");
            //DataProvider.requestJoinRecordList = result;
            //Log.v("dp.JoinRecordList", "" + DataProvider.requestJoinRecordList.size());
        }
    }
}
