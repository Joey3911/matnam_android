package com.soma.second.matnam.ui;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.MatnamApi;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.LikeRoomRecord;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.RequestJoinRecord;
import com.soma.second.matnam.R;
import com.soma.second.matnam.Utils.CloudEndpointBuildHelper;
import com.soma.second.matnam.listdubbies.provider.DataProvider;
import com.soma.second.matnam.ui.adapters.MyRoomListAdapter;
import com.soma.second.matnam.ui.models.User;
import com.soma.second.matnam.ui.widget.Indicator;
import com.soma.second.matnam.ui.widget.JoinRequestListDialog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyInfoActivity extends AppCompatActivity {

    Indicator mIndicator;
    ViewGroup mLayout;
    ListView myRoomList;
    MatnamApi matnamApi = null;
    ArrayList<Object> likeRoomList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_info);

        if (matnamApi == null) {
            matnamApi = CloudEndpointBuildHelper.getEndpoints();
        }
        likeRoomList = new ArrayList<Object>();

        mIndicator = new Indicator(this);
        mLayout = (ViewGroup) findViewById(R.id.container);

        TextView fullNameTextView = (TextView) findViewById(R.id.myinfo_full_name);
        fullNameTextView.setText(User.getFullName());

        myRoomList = (ListView) findViewById(R.id.myinfo_myroom_list);
        myRoomList.setBackgroundColor(getResources().getColor(R.color.deep_orange_400));
        myRoomList.setDivider(null);

        ImageView profileImgView = (ImageView) findViewById(R.id.myinfo_profile_img);
        Glide.with(this).load(User.getProfileImgUrl()).into(profileImgView);

        // 모든 LikeRoomRecordList 가져오기
        List<LikeRoomRecord> likeRoomRecordList = DataProvider.likeRoomRecordList;
        final ArrayList<LikeRoomRecord> mMyRoom = new ArrayList<LikeRoomRecord>();

        for (int i = 0; i < likeRoomRecordList.size(); i++) {
            LikeRoomRecord likeRoomRecord = likeRoomRecordList.get(i); //
            Log.v("List.get(i)",likeRoomRecord.toString());
            String membersId = likeRoomRecord.getMembersId();
            Log.v("membersId",membersId);
            String[] memberIdArr = membersId.split(",");
            Log.v("memberIdArr[]",memberIdArr.toString());

            if(memberIdArr[0].equals(User.getId())) {
                mMyRoom.add(likeRoomRecord);
            }
        }

        myRoomList.setAdapter(new MyRoomListAdapter(this, mMyRoom));

        myRoomList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long l_position) {
                //Toast.makeText(getApplicationContext(), mMyRoom.get(position).getTitle(), Toast.LENGTH_SHORT).show();
                JoinRequestListDialog joinRequestListDialog = new JoinRequestListDialog(MyInfoActivity.this, mMyRoom.get(position));
                Log.v("room_id", mMyRoom.get(position).getId().toString());
                joinRequestListDialog.show();
            }

        });

        new loadRequestJoinDataAsyncTask().execute();
    }

    class loadRequestJoinDataAsyncTask extends AsyncTask<Void, Void, List<RequestJoinRecord>> {

        List<RequestJoinRecord> requestJoinRecordList;
        @Override
        protected List<RequestJoinRecord> doInBackground(Void... params) {
            if (matnamApi == null) {
                matnamApi = CloudEndpointBuildHelper.getEndpoints();
            }
            try {
                requestJoinRecordList = matnamApi.getRequestJoins().execute().getItems(); // 서버에서 joinRequest 데이터 가져오기
            } catch (IOException e) {
                e.printStackTrace();
            }

            return requestJoinRecordList;
        }

        @Override
        protected void onPostExecute(List<RequestJoinRecord> result) {
            super.onPostExecute(result);
        }
    }
}
