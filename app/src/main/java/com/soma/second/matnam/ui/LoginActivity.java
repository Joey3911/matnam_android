package com.soma.second.matnam.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.kimyoungjoon.myapplication.backend.matnamApi.MatnamApi;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.LikeRoomRecord;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.PlaceRecord;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.UserRecord;
import com.faradaj.blurbehind.BlurBehind;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.soma.second.matnam.R;
import com.soma.second.matnam.Utils.CloudEndpointBuildHelper;
import com.soma.second.matnam.Utils.InstagramRestClient;
import com.soma.second.matnam.listdubbies.provider.DataProvider;
import com.soma.second.matnam.ui.InstagramApp.OAuthAuthenticationListener;
import com.soma.second.matnam.ui.models.InstagramFollwer;
import com.soma.second.matnam.ui.models.User;
import com.soma.second.matnam.ui.widget.Indicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.soma.second.matnam.Utils.Utils.loadBitmap;

public class LoginActivity extends Activity implements View.OnClickListener {

	Indicator mIndicator;
	MatnamApi matnamApi = null;
	List<PlaceRecord> places;

	private InstagramApp mApp;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);
		blurBehindBackAcitivity();

		mIndicator = new Indicator(this);

		mApp = new InstagramApp(this, ApplicationData.CLIENT_ID, ApplicationData.CLIENT_SECRET, ApplicationData.CALLBACK_URL);
		mApp.setListener(listener);

		Button loginButton = (Button) findViewById(R.id.loginButton);
		loginButton.setOnClickListener(this);

		if (mApp.hasAccessToken()) {
			loginButton.setVisibility(View.INVISIBLE);
			User.setId(mApp.getId());
			new initLoadingAsyncTask().execute(mApp.getId());
		} else {
			loginButton.setVisibility(View.VISIBLE);
		}


	}

	public void blurBehindBackAcitivity() {
		BlurBehind.getInstance()
				.withAlpha(150)
				.withFilterColor(Color.parseColor("#696969"))
				.setBackground(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.loginButton:
				//new initLoadingAsyncTask().execute("testID");

				if (mApp.hasAccessToken()) {
					final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);

					builder.setMessage("Disconnect from Instagram?")
							.setCancelable(false)
							.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									mApp.resetAccessToken();
								}
							})
							.setNegativeButton("No", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									dialog.cancel();
								}
							});

					final AlertDialog alert = builder.create();
					alert.show();
				} else {
					mApp.authorize();
				}
				break;
		}
	}

	OAuthAuthenticationListener listener = new OAuthAuthenticationListener() {

		@Override
		public void onSuccess() {
			// TODO Auto-generated method stub
			//Go To NextActivity
			User.setId(mApp.getId());
			Log.e("TEST", mApp.getId() + "  getID");

//			Intent intent = new Intent(LoginActivity.this, RegistrationIntentService.class);
//			intent.putExtra("id", mApp.getId());
//			startService(intent);

			new initLoadingAsyncTask().execute(mApp.getId());
		}

		@Override
		public void onFail(String error) {
			// TODO Auto-generated method stub
			Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
		}

	};

	class initLoadingAsyncTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {

			String token = null;

			try{
				InstanceID instanceID = InstanceID.getInstance(LoginActivity.this);
				token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
						GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
				// [END get_token]
				Log.i("RegIntentService", "GCM Registration Token: " + token);
			}catch (Exception e) {
				Log.d("RegIntentService", "Failed to complete token refresh", e);
				// If an exception happens while fetching the new token or updating our registration data
				// on a third-party server, this ensures that we'll attempt the update at a later time.
//            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).apply();
			}


			if (matnamApi == null) {
				matnamApi = CloudEndpointBuildHelper.getEndpoints();
			}

			UserRecord newUser = new UserRecord();

			newUser.setGender(true);
			newUser.setId(params[0]);
			newUser.setToken(token);
			newUser.setName(User.getUserName());

			try {
				matnamApi.addUser(newUser).execute();
				places = matnamApi.getPlaces().execute().getItems();
			} catch (IOException e) {
				Log.e("API", "Error" + e.getMessage());
				e.printStackTrace();
			}

			int index = 0;
			for (Iterator<PlaceRecord> iter = places.iterator(); iter.hasNext(); ) {
				PlaceRecord place = iter.next();

				long foodId = place.getId();
				String foodKeyword = place.getKeyword();
				String lowImgUrl = place.getLowResolution();

				if (index < 10) {
					DataProvider.foodId_left[index] = foodId;
					DataProvider.foodKeyword_left[index] = foodKeyword;
					DataProvider.foodImgUrl_left[index] = lowImgUrl;
				} else if (index < 20 && index >= 10) {
					DataProvider.foodId_right[index - 10] = foodId;
					DataProvider.foodKeyword_right[index - 10] = foodKeyword;
					DataProvider.foodImgUrl_right[index - 10] = lowImgUrl;
				}
				index++;
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			String UserUrl = InstagramRestClient.userInfo(User.getId());
			InstagramRestClient.get(UserUrl, null, new JsonHttpResponseHandler() {

				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

					try {
						JSONObject data = response.getJSONObject("data");
						String id = data.getString("id");
						String fullName = data.getString("full_name");
						String userName = data.getString("username");
						String profileImgUrl = data.getString("profile_picture");

						User.setFullName(fullName);
						User.setUserName(userName);
						User.setProfileImgUrl(profileImgUrl);

					} catch (JSONException e) {
						e.printStackTrace();
					}

				}

			});

			String FollowerUrl = InstagramRestClient.userFollows(User.getId());
			InstagramRestClient.get(FollowerUrl, null, new JsonHttpResponseHandler() {

				@Override
				public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

					try {
						JSONArray dataArr = response.getJSONArray("data");
						for (int i = 0; i < dataArr.length(); i++) {
							JSONObject data = (JSONObject) dataArr.get(i);
							String id = data.getString("id");
							String fullName = data.getString("full_name");
							String userName = data.getString("username");
							String profileImgUrl = data.getString("profile_picture");

							DataProvider.instagramFollwerList.add(new InstagramFollwer(id, fullName, userName, profileImgUrl));
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}

			});

			new loadLikeRoomDataAsyncTask().execute();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (!mIndicator.isShowing())
				mIndicator.show();
		}
	}

	class loadLikeRoomDataAsyncTask extends AsyncTask<String, Void, List<LikeRoomRecord>> {

		List<LikeRoomRecord> likeRoomRecordList;

		@Override
		protected List<LikeRoomRecord> doInBackground(String... params) {
			if (matnamApi == null) {
				matnamApi = CloudEndpointBuildHelper.getEndpoints();
			}

			try {
				likeRoomRecordList = matnamApi.getLikeRooms().execute().getItems();
			} catch (IOException e) {
				Log.e("API", "Error" + e.getMessage());
				e.printStackTrace();
			}
			return likeRoomRecordList;
		}

		@Override
		protected void onPostExecute(List<LikeRoomRecord> resultList) {
			super.onPostExecute(resultList);
			DataProvider.likeRoomRecordList = resultList;

			for(LikeRoomRecord likeRoomRecord : resultList) {
				long placeId = likeRoomRecord.getPlaceId();
				if(DataProvider.likeRoomRecordMap.containsKey(placeId)) {
					List<LikeRoomRecord> existRoomList = DataProvider.likeRoomRecordMap.get(placeId);
					existRoomList.add(likeRoomRecord);
					DataProvider.likeRoomRecordMap.put(placeId, existRoomList);
				} else {
					List<LikeRoomRecord> newRoomList = new ArrayList<>();
					newRoomList.add(likeRoomRecord);
					DataProvider.likeRoomRecordMap.put(placeId, newRoomList);
				}

				if(!DataProvider.likeRoomFoodImgMap.containsKey(placeId))
					new loadFoodImgAsyncTask(placeId).execute();
			}


			Handler mHandler = new Handler();
			mHandler.postDelayed(new Runnable() {
				//Do Something
				@Override
				public void run() {
					if (mIndicator.isShowing())
						mIndicator.hide();

					Intent intent = new Intent(LoginActivity.this, MainActivity.class);
					startActivity(intent);
					finish();
				}
			}, 1000 * 3);

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	class loadFoodImgAsyncTask extends AsyncTask<Long, Void, Bitmap> {

		long placeId;

		loadFoodImgAsyncTask(Long _placeId) {
			this.placeId = _placeId;
		}

		@Override
		protected Bitmap doInBackground(Long... params) {

			PlaceRecord place = null;
			try {
				place = matnamApi.getPlace(placeId).execute();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return loadBitmap(place.getThumbnail());
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
			if(!DataProvider.likeRoomFoodImgMap.containsKey(placeId))
				DataProvider.likeRoomFoodImgMap.put(placeId, result);
		}
	}

}
