package com.soma.second.matnam.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.kimyoungjoon.myapplication.backend.matnamApi.MatnamApi;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.LikeRoomRecord;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.RequestJoinRecord;
import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.UserRecord;
import com.soma.second.matnam.R;
import com.soma.second.matnam.Utils.CloudEndpointBuildHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.io.Resources.getResource;

/**
 * Created by youngjoosuh on 2015. 11. 13..
 */
public class joinRequestListDialogAdapter extends BaseAdapter {
    private Context context;
    private List<RequestJoinRecord> data;
    MatnamApi matnamApi;

    public joinRequestListDialogAdapter(Context context, List<RequestJoinRecord> data) {
        if(data==null) Log.v("arraydata", "null");
        else Log.v("arraydata", ""+data.size());
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        if(data==null) {
            return 0;
        } else {
            return data.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if(data==null) {
            return null;
        } else {
            return data.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        public TextView tv;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (matnamApi == null) {
            matnamApi = CloudEndpointBuildHelper.getEndpoints();
        }
        if(convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.myroom_list_item, null);
            holder.tv = (TextView) convertView.findViewById(R.id.textView);
            holder.tv.setBackgroundColor(Color.WHITE);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String str = null;
        String[] members = null;

        if(data !=null) {
            str = data.get(position).getRequestMembersId();
            members = str.split(",");
        }
        if(str==null || str.equals("")) {
            holder.tv.setText("NO MEMBERS_ID");
        } else {
            holder.tv.setText("Join Request" + (position+1));
        }
        return convertView;
    }
}
