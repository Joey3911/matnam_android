package com.soma.second.matnam.ui.advrecyclerview.data;

/**
 * Created by youngjoosuh on 2015. 11. 15..
 */

import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.RequestJoinRecord;

import java.util.ArrayList;
public class MyRoomDataProvider {

    RequestJoinRecord requestJoinRecord;
    public static ArrayList<RequestJoinRecord> myRoomJoinRequest = new ArrayList<RequestJoinRecord>();
    //public static ArrayList<String> myRoomJoinRequest;


    public ArrayList<RequestJoinRecord> getJoinRequestByRoomId(long room_id) {
        ArrayList<RequestJoinRecord> temp = null;
        for(int i=0; i<=myRoomJoinRequest.size(); i++) {
            if(myRoomJoinRequest.get(i).getRoomId()==room_id) {
                temp.add(myRoomJoinRequest.get(i));
            }
        }
        return temp;
    }

    public void setJoinRequest(RequestJoinRecord requestJoinRecord) {
        RequestJoinRecord temp = requestJoinRecord;
        myRoomJoinRequest.add(temp);
    }


}
