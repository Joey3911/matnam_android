/*
 *    Copyright (C) 2015 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.soma.second.matnam.ui.advrecyclerview.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.example.kimyoungjoon.myapplication.backend.matnamApi.model.LikeRoomRecord;
import com.soma.second.matnam.listdubbies.provider.DataProvider;
import com.soma.second.matnam.ui.advrecyclerview.data.AbstractExpandableDataProvider;
import com.soma.second.matnam.ui.advrecyclerview.data.LikeRoomDataProvider;

import java.util.ArrayList;
import java.util.List;

public class LikeRoomDataProviderFragment extends Fragment {

    private LikeRoomDataProvider mDataProvider;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);  // keep the mDataProvider instance
        if(LikeRoomDataProvider.PLACE_ID == 0) {
            mDataProvider = new LikeRoomDataProvider(DataProvider.likeRoomRecordList);
//            Log.e("LikeRoomDataProvider", "All : " + DataProvider.likeRoomRecordList.toString());
        } else {
            List<LikeRoomRecord> likeRoomRecordList = DataProvider.likeRoomRecordMap.get(LikeRoomDataProvider.PLACE_ID);
            if(likeRoomRecordList != null)
                mDataProvider = new LikeRoomDataProvider(DataProvider.likeRoomRecordMap.get(LikeRoomDataProvider.PLACE_ID));
            else {
                LikeRoomRecord likeRoomRecord = new LikeRoomRecord();
                likeRoomRecord.setId((long) 0);
                likeRoomRecord.setPlaceId((long) 0);
                likeRoomRecord.setTitle("방이 없습니다. 새로 만들어주세요!");
                likeRoomRecord.setDate("맛남");
                likeRoomRecord.setMembersId("");
                likeRoomRecord.setMemberCount(0);

                List<LikeRoomRecord> emptyLikeRoomRecordList = new ArrayList<>();
                emptyLikeRoomRecordList.add(likeRoomRecord);
                mDataProvider = new LikeRoomDataProvider(emptyLikeRoomRecordList);
            }
//            Log.e("LikeRoomDataProvider", LikeRoomDataProvider.PLACE_ID + " : " + DataProvider.likeRoomRecordMap.get(LikeRoomDataProvider.PLACE_ID).toString());
        }

    }
    public AbstractExpandableDataProvider getDataProvider() {
        return mDataProvider;
    }
}
