package com.example.kimyoungjoon.myapplication.backend.models;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * Created by kimyoungjoon on 2015. 10. 31..
 */
@Entity
public class RequestJoinRecord {
    @Id
    private Long id;            //  요청 id
    private Long room_id;       //  LikeRoom id
    private String receive_members_id;  //  LikeRoom 소유자 id
    private String request_members_id;  //  요청 신청자 id

    public RequestJoinRecord(Long room_id, String receive_members_id, String request_members_id) {
        this.room_id = room_id;
        this.receive_members_id = receive_members_id;
        this.request_members_id = request_members_id;
    }

    public RequestJoinRecord(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoom_id() {
        return room_id;
    }

    public void setRoom_id(Long room_id) {
        this.room_id = room_id;
    }

    public String getReceive_members_id() {
        return receive_members_id;
    }

    public void setReceive_members_id(String receive_members_id) {
        this.receive_members_id = receive_members_id;
    }

    public String getRequest_members_id() {
        return request_members_id;
    }

    public void setRequest_members_id(String requst_members_id) {
        this.request_members_id = requst_members_id;
    }
}
