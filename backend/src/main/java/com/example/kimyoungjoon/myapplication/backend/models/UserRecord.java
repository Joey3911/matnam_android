package com.example.kimyoungjoon.myapplication.backend.models;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * Created by kimyoungjoon on 2015. 10. 31..
 */
@Entity
public class UserRecord {
    @Id
    private String id;      //  User id
    private String name;    //  이름
    private boolean gender; //  성별
    private int age;        //  나이대
    private String token;   //  GCM 토큰

    public UserRecord(String id, String name, boolean gender, int age, String token) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.token = token;
    }

    public UserRecord(){

    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
